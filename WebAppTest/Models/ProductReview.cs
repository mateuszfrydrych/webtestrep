﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAppTest.Models
{
    public class ProductReview
    {
        public int Id { get; set; }
        [Required]
        [StringLength(1024)]
        public string Review { get; set; }
        [Display(Name ="User Name")]
        [DisplayFormat(NullDisplayText ="anonymous")]
        public string ReviewerName { get; set; }
        [Range(1,10)]
        [Required]
        public int Rating { get; set; }
        public int ProductId { get; set; }
    }
}