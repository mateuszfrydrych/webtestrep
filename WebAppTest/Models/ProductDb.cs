﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebAppTest.Models
{
    public class ProductDb : DbContext
    {
        public ProductDb() : base("name=DefaultConnection")
        {

        }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductReview> Reviews { get; set; }
    }
}