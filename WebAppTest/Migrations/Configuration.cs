using System.Collections.Generic;

namespace WebAppTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<WebAppTest.Models.ProductDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(WebAppTest.Models.ProductDb context)
        {
            context.Products.AddOrUpdate(p => p.Name,
                new Models.Product { Name = "Produkt 1", Description = "Opis 1", Price = 1.5M },
                new Models.Product { Name = "Produkt 2", Description = "Opis 2", Price = 2.5M },
                new Models.Product
                {
                    Name = "Produkt 3",
                    Description = "Opis 3",
                    Price = 3.5M,
                    Reviews = new List<Models.ProductReview>
                    {
                        new Models.ProductReview{Review="Pyszny",ReviewerName="Czesiek",Rating = 8}
                    }
                });

            for(int i =0; i<1000; i++)
            {
                context.Products.AddOrUpdate(r => r.Name,
                    new Models.Product { Name = i.ToString() + " Produkt", Description = "Opis" + i, Price = (decimal)i});
            }
        }
    }
}
