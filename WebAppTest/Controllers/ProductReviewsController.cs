﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebAppTest.Models;

namespace WebAppTest.Controllers
{
    public class ProductReviewsController : Controller
    {
        private ProductDb db = new ProductDb();

        // GET: ProductReviews
        public ActionResult Index([Bind(Prefix ="id")]int ProductId)
        {
            var product = db.Products.Find(ProductId);
            if(product != null)
            return View(product);
            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult Create(int ProductId)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ProductReview ProductReview)
        {
            if (ModelState.IsValid)
            {
                db.Reviews.Add(ProductReview);
                db.SaveChanges();
                return RedirectToAction("Index", new { id = ProductReview.ProductId });
            }
            return View(ProductReview);
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = db.Reviews.Find(id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(ProductReview ProductReview)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ProductReview).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = ProductReview.ProductId });
            }
            return View(ProductReview);
        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
