using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppTest.Models;
using System.Data.Entity;
using PagedList;

namespace WebAppTest.Controllers
{
    public class HomeController : Controller
    {
        ProductDb _db = new ProductDb();
        public ActionResult Autocomplete(string term)
        {
            var model = _db.Products.Where(p => p.Name.StartsWith(term)).Take(10).Select(r => new { label = r.Name });

            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Index(string searchTerm = null,int page =1)
        {
            var model = _db.Products.OrderByDescending(p => p.Reviews.Average(r => r.Rating)).Where(x=>x.Name.StartsWith(searchTerm)||searchTerm==null).ToPagedList(page,10);
            if (Request.IsAjaxRequest())
            {
                return PartialView("_Products", model);
            }
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if(_db != null)
            {
                _db.Dispose();                
            }
            base.Dispose(disposing);
        }
    }
}